#include "stdafx.h"
#include <Windows.h>
#include <stdlib.h>
#include "opencv/cv.h"
#include "opencv2/imgproc/imgproc.hpp"
#include <string.h>
#include <set>
#include<algorithm>
#include <string>
#include <iterator>
#include<vector>
#include<fstream>
#include<string.h>
#include<math.h>
#include <tuple>

//#include "boost/range/algorithm/set_algorithm.hpp"

#include "opencv2/highgui/highgui.hpp"
#include <iostream>



#pragma warning (disable : 4996)
using namespace cv;
using namespace std;


Mat image_preprocess(Mat);
int edge_position(set<int>, set<int>);
set<int> add_rawpixel(Mat);
double length(int);
Mat takeSlice(Mat, int);
tuple<Point, Point> searchMatch(Mat, Mat);

int col1 = 960;



//------------------START ----MAIN() ---- FUNCTION------------------------
int main()
{
	char line[40], file1[40], file2[40], line2[40]; 
	string detail, truck;
	int length_estimate = 0, count = 1;
	double l, car,l_truck;
	std::ifstream listfile("temp.txt"); // read image names from the text file
	std::ofstream example("temp1.txt", ios::out);
	//string  file1,file2;
	Mat P_image, P_image1, P_image2, s_image, img1, img2,refslice;
	set<int> x1, x2;
	if (listfile.is_open())
	{
		while (!listfile.eof())
		{
			listfile.getline(line, 40);
			int len = strlen(line);
			strcpy(file1, line);
			cout << file1;
			cout << "\t";
			P_image = imread(line, 0);

			if (P_image.empty()) //check whether the image is loaded or not
			{
				cout << "Error : Image cannot be loaded..!!" << endl;
				return -1;
			}
			 P_image(Rect(256, 0, 768, 960));
			 imshow("input", P_image);
			//double t = (double)getTickCount();
			// Process iamge
			 img1 = image_preprocess(P_image);
			x1 = add_rawpixel(img1);
			int length_of_car = 0;
			if (x1.size() < 200)
			{
				cout << "Vehical not present in frame1 \t" << file1 << endl;
			}
			boolean flag = false;
			while (!listfile.eof())
			{
				listfile.getline(line, 40);
				strcpy(file2, line);
				cout << file2;
				s_image = imread(line, 0);
				if (s_image.empty()) //check whether the image is loaded or not
				{
					cout << "Error : Image cannot be loaded..!!" << endl;
					return -1;
				}
				 s_image(Rect(256, 0, 768, 960));
				 imshow("input 1", s_image);
				double t = (double)getTickCount();
				img2 = image_preprocess(s_image);
				x2 = add_rawpixel(img2);
				cout << "\tValue of X2=" << to_string(x2.size()) <<"\t";
				if (flag == false)
				{
					if (x2.size() < 200)
					{
						cout << "\nVehical not present in frame2\t" << file2 << endl;
					}
					else
					{
						int a = edge_position(x1, x2);
						if (a != 0)
						{
							cout << "position of image" << to_string(a);
							l = length(a) - length(960);
							flag = true;
						}
					}
					P_image = s_image;
					img1 = img2;
					x1 = x2;
					//file1 = file2;
					
				}

				if (flag == true)
				{
					if (x2.size() > 500)
					{
						cout << "current vechical can be car or truck" << endl;
						int max_len = x1.size();
						if (max_len < x2.size())
						{
							if (x2.size() < 700)
							{
								cout << "It's a Car" << endl;
								set<int> ::iterator i;
								for (i = x2.begin(); i != x2.end(); i++)
								{
									//t = x2[i] -x2[i-1];
									//if (x2[i]>500 and t>20) :
									auto a = *i, b = (*i - 1);
									auto t = a - b;

									if (a > 500 && t > 20)
									{
										auto maxpix = *i;
									}
									car = l + length(338) - length(a);
								}
								cout << "\n \n Length of car =" << to_string(car);
								if (length_of_car < car)
								{
									length_of_car = car;
									/*cout << "Length of car =" << to_string(length_of_car);
									detail = file2 + to_string(length_of_car);
									cout << detail; */
								}
							}
						}
					}
				}
				tuple<Point>minloc,maxloc;
				
				if (x2.size() >800)
				{
					cout << "It's Truck or Long Vehicle" << endl;
					l_truck = l + length(337) - length(960);
					cout << "111111";
					P_image = s_image;   //P_image1 instead of P_image
					cout << "222222";
					img1 = img2;
					cout << "333333";
				//	file1 = file2;
					truck = file1;
					cout << "444444";
					refslice = takeSlice(P_image, 940);  //P_image1 instead of P_image
					cout << "55555";
					while (x2.size() > 800)
					{
						cout << "666666";
						listfile.getline(line2, 40);
						strcpy(file2, line2);
						cout << file2;
						s_image = imread(file2, 0);
						img2 = image_preprocess(s_image);
						x2 = add_rawpixel(img2);
						minloc, maxloc = searchMatch(s_image, refslice);
					//	length_estimate = length_estimate + (length(get<1>(minloc)) - length(960));
						P_image = s_image;  //P_image1 instead of P_image
						img1 = img2;
						//file1 = file2;
						x1 = x2;
						refslice = takeSlice(P_image, 940);  //P_image1 instead of P_image
						count = count + 1;
						l_truck = l_truck + length_estimate / (count / 2);
						cout << "Length of truck=" << to_string(l_truck);
						detail = truck + to_string(l_truck);
						cout << "Detail= " << detail << endl;
						//imshow("ref", refslice);
					}
				}

				else
				{
					if (x2.size() < 200)
					{
						cout << "-------------------";
						cout << "There is No vehicle";
						flag = false;
					}
				}
				P_image = s_image;
				img1 = img2;
				x1 = x2;
				//	file1 = file2;
				// end process image
				t = ((double)getTickCount() - t) / getTickFrequency();
				imshow("out1", img1);
				cout << "T(sec) = " << t << endl;
				waitKey(500);
			}  //end of nested while
		} //end of first while
		listfile.close();	
	} //end of first if
	if (P_image.empty()) //check whether the image is loaded or not
	{
		cout << "Error : Image cannot be loaded..!!" << endl;
		return -1;
	}
	return 0;
}
//------------------END ----MAIN() ---- FUNCTION------------------------



//------------------START ----FUNCTION-----IMAGE_PREPROCESS()-------------------

Mat image_preprocess(Mat image)
{
	Mat ims1,dst;
	Mat kernel;
	kernel = Mat::ones(2, 2 , CV_32F) / 25;
	filter2D(image, dst, -1, kernel);
	//blur(image, dst, Size(3, 3));
	Mat edges, ims;
	Canny(dst, edges, 75, 35);
	imshow("ssss", dst);
	return edges;
}
//------------------END ----FUNCTION-----IMAGE_PREPROCESS()-------------------


//------------------START ----FUNCTION-----ADD_RAW_PIXEL()-------------------

set<int> add_rawpixel(Mat img)
{
	set<int> x;
	Mat image;
	img.convertTo(image, CV_8UC1);
	int col = image.cols, row = image.rows;
	Mat data = Mat::zeros(col, 5, CV_8UC1);
	
	
	int  j,k,count[50][50];
	for (int i = 0; i < col; ++i)
	{
		int j = 1;
		while (j <= 5)
		{
			
			for (k = (j - 1)*row / 5; k < j*row / 5; k++)
			{
			int	a  = (int)image.at<uchar>(i, k);
			data.at<uchar>(i, j - 1) = (int)data.at<uchar>(i, k) + a;
				//count[i][k] = sum();//(image.at<uchar>(i, k));
			}
		}
	 }
	
	for (int i = 959; i >= 0; i--)
	{
		if (data.at<uchar>(i, 1) > 0 || data.at<uchar>(i, 2) > 0 || data.at<uchar>(i, 3) > 0)
			{
				x.insert(i);
			}
	}
	return x;
}
/*
	for (int i = 0; i <= col; ++i)
	{
		int j = 1;
		while (j <= 5)
		{
			int sum = 0, k;
			for (k = (j - 1)*row / 5; k < j*row / 5; k++)
			{
				sum = sum + k;    //data[i, j - 1] = np.sum(img[i, ((j - 1)*row / 5) : (j*row / 5)])
			}
			data = Mat(i, k, CV_16U, sum);
			j = j + 1;
			vector <int> x;
			for (i = 959; i >= 0; i--)
			{
				//if (Point2f data(i, 1)!=0;)
				//vector<float> v;
				//v.push_back((float)CV_PI);   v.push_back(2);    v.push_back(3.01f);
				//	if (v != 0;)

				if (data[i, 1] != 0 || data[i, 2] != 0 || data[i, 3] != 0)
			}
			//cv::Mat sub_mat_1 = mat(cv::Range(0, 106), cv::Range::all());
			//cv::Mat sub_mat_2 = mat(cv::Range(106, 151), cv::Range::all());

			//std::cout << cv::sum(sub_mat_1).val[0] << std::endl;
			//std::cout << cv::sum(sub_mat_2).val[0] << std::endl;


		}
	}

}


*/

/*
set<int> add_rawpixel(Mat img)
{
	set<int> x;
	Mat image;
	img.convertTo(image, CV_8U);
	int col = image.cols, row = image.rows;

	int i, j,count;
	for ( i = 0; i < row; i++)
	{
		count = 0;
		for (j = 0; j< col; j++)
		{
			if (image.at<uchar>(i, j) > 0)
			{
				count += image.at<uchar>(i, j);
			}
		}
		if (count>0)
		{
			x.insert(i);
		}
	}
	//cout << "count=" << x[0];
	return x;
}*/
//------------------END ----FUNCTION-----ADD_RAW_PIXEL()-------------------


//------------------START ----FUNCTION-----EDGE_POSITION()-------------------
int edge_position(set<int> x1, set<int>x2)
{
	set <int> x;
	set_difference(x1.begin(), x1.end(), x2.begin(), x2.end(), std::inserter(x, x.begin()));
	int count_l = 1;
	//for (vector<int>::const_iterator i = x.begin(); i != x.end(); ++i)
	set<int> ::iterator i;
	for (i = x.begin(); i != x.end(); ++i)
	{
		cout << *i << endl;
		if (*i > col1 / 2)
		{
			break;
		}
		count_l = count_l + 1;

		if (count_l < x.size())
		{
			return *i;
		}
		return 0;
	}
}
//------------------END ----FUNCTION-----EDGE_POSITION()-------------------

//------------------START ----FUNCTION-----LENGTH()-------------------
double length(int HeightPix)
{
	//double f = 0.12;
	double fCamera_Height = 6.55;
	double fCamera_Angle_Vertical = 28.0;
	double fCamera_Angle_Horizontal = 0.0;
	double fSensor_Height = 0.0036;
	double fFocal_Length = 0.0169 * 1.44;
	double f = 0.165;
	double rows = 960;
	double sensorWidth = 0.048;
	double sensorHeight = 0.036;
	//	double alpha = 0.4677;
	double alpha = 0.48869;// 28 degrees
	double betaH = atan(sensorWidth / (2 * f));
	double height = 6.75; //in meter;
	double betaV = atan(fSensor_Height / (2 * fFocal_Length)); // In radians
	double y = double(rows - double(HeightPix)) / double(rows);
	double gamma = atan((2 * y - 1)*tan(betaV));
	double fCamera_Angle_Vertical_radians = fCamera_Angle_Vertical*0.0174532925;
	double distance = (fCamera_Height / tan(fCamera_Angle_Vertical_radians - gamma));
	//print str(HeightPix) + "\t" + str(distance)
	return distance;
}
//------------------END ----FUNCTION-----LENGTH()-------------------


//------------------START ----FUNCTION-----TAKESLICE()-------------------

Mat takeSlice(Mat img, int pos)
{
	
	int col = img.cols, row = img.rows;
	Mat slice;
	

	//Size sz = img.size();
	if (pos > 0 && pos < row)
	{
		for (int i = 0; i < col; ++i)
		{
			img.at<uchar>(pos, i);
		}
	}
	return slice;
}

//------------------END ----FUNCTION-----TAKESLICE()-------------------		


//------------------START ----FUNCTION-----SEARCH_MATCH()-------------------

tuple<Point,Point> searchMatch(Mat img, Mat ref)
{
	int row = img.rows, col = img.cols;
	Mat imgf,refF,res,ims1;
	img.convertTo(imgf, CV_32F);
	ref.convertTo(refF, CV_32F);
	matchTemplate(imgf, refF, res, CV_TM_CCOEFF);
	double minVal, maxVal;
	Point minLoc, maxLoc,topLeft,bottomRight;
	minMaxLoc(res, &minVal, &maxVal, &minLoc, &maxLoc);
	topLeft = maxLoc;
	bottomRight = topLeft + Point(ref.cols,  ref.rows);
	rectangle(img, topLeft, bottomRight, 255, 1);
	resize(img, ims1,Size(640, 540));
	imshow("out2", ims1);
	return tuple<Point,Point>(minLoc, maxLoc);
}
//------------------END ----FUNCTION-----SEARCH_MATCH()-------------------


//------------------START ----FUNCTION-----EDGE_DETECTION()--------- already IMPLEMENTED & USED in main method ----------


/*def edge_detection(listfile) :
line1 = listfile.readline()
file = line1[0:len(line1) - 1]
image1 = cv2.imread(dir + file)
img1 = image_preprocess(image1)
x1 = add_rawpixel(img1)
while True:
line2 = listfile.readline()
if not line2 : break
file = line2[0:len(line2) - 1]
image2 = cv2.imread(dir + file)
img2 = image_preprocess(image2)
x2 = add_rawpixel(img2)
a = edge_position(x1, x2)
if (a != 0) :
return line1, line2, a
break
else:
x1 = x2
line1 = line2
return 0, 0, 0
*/
//------------------START ----FUNCTION-----EDGE_DETECTION()-------------------