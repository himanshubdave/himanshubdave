// Modified for Linux --------------------------------
//#include "stdafx.h"
//#include <Windows.h>
#include <stdlib.h>
#include "opencv/cv.h"
#include "opencv2/imgproc/imgproc.hpp"
#include <string.h>
#include <set>
#include <algorithm>
#include <string>
#include <iterator>
#include <vector>
#include <fstream>
#include <string.h>
#include <math.h>
#include <tuple>

//#include "boost/range/algorithm/set_algorithm.hpp"

#include "opencv2/highgui/highgui.hpp"
#include <iostream>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/core.hpp"
#include <opencv2/core/utility.hpp>
#include <unistd.h>  // for usleep()

using namespace cv;
using namespace std;

Mat image_preprocess(Mat);
int edge_position(set<int>, set<int>);
set<int> add_rawpixel(Mat);
double length(int );
Mat takeSlice(Mat, int);
tuple<Point, Point> searchMatch(Mat, Mat);


int col1 = 960;
int THR = 49;

//------------------START ----MAIN() ---- FUNCTION------------------------
int main()
{
	char line[40], file1[40], file2[40];
	std::ifstream listfile("imagedata1.txt"); // read image names from the text file
	//string  file1,file2;
	Mat P_image, P_image1, P_image2, s_image, img1, img2;
	set<int> x1, x2;
	if (listfile.is_open())
	{
		if(!listfile.eof()) // it was wrongly while
		{
			 listfile.getline(line, 40);
			 //int len = strlen(line);
			 strcpy(file1, line);
			 cout << file1;
			 cout << "\t";
			 P_image = imread(line, 0);

			if (P_image.empty()) //check whether the image is loaded or not
			{
				cout << "Error : Image cannot be loaded..!!" << endl;
				return -1;
			}
			P_image1 = P_image(Rect(340, 0, 580, 960));
			imshow("input", P_image1);
			// double t = (double)getTickCount(); // not here!
			// Process iamge
			img1 = image_preprocess(P_image1);
			x1 = add_rawpixel(img1);
			cout << "x1 = " << x1.size() << endl;
			int length_of_car = 0;
			if (x1.size() < THR) // was 200
			{
				cout << "Vehical not present in frame1 \t" << file1 <<endl;
			}	

			bool flag = false;
			while (!listfile.eof())
			{
				listfile.getline(line, 40);
				strcpy(file2, line);
				cout << file2 << "\t";
				s_image = imread(line,0);
				if (s_image.empty()) //check whether the image is loaded or not
				{
					cout << "Error : Image cannot be loaded..!!" << endl;
					return -1;
				}
				P_image2 = s_image(Rect(340, 0, 580, 960));
				imshow("input", P_image2);
				double t = (double)getTickCount(); // should be here!
				img2 = image_preprocess(P_image2);
				x2 = add_rawpixel(img2);
				cout << "x2 = " << x2.size() << endl;
				if (flag == false)
				{
					// flag = true; // do we need this?
					if (x2.size() < THR) // was 200
					{
						cout << "Vehical not present in frame2 \t" << file2<<endl;
					}
					else
					{
						int a = edge_position(x1, x2);
						if (a != 0)
						{
							cout << "-----> position of image " << to_string(a) << endl;
							double l = length(a) - length(960);
                            cout << "l = " << to_string(l) << endl; 
							flag = true;
						}	
					}
				} // **
						P_image = s_image;
						img1 = img2;
						x1 = x2;
					//	file1 = file2;
				//} --> **
			//}  this was in wrong position	
			// end process image
				t = ((double)getTickCount() - t) / getTickFrequency();
				imshow("out1", img1);
				cout << "T(sec) = " << t << endl;
				waitKey();
			}  // this should be end of the while loop
		}
		listfile.close();	
	}
	// if (P_image.empty()) //check whether the image is loaded or not
	//{
	//	cout << "Error : Image cannot be loaded..!!" << endl;
	//	return -1;
	//}
	return 0;
}
//------------------END ----MAIN() ---- FUNCTION------------------------



//------------------START ----FUNCTION-----IMAGE_PREPROCESS()-------------------

Mat image_preprocess(Mat image)
{
	Mat ims1,dst;
	//Mat kernel;
	//kernel = Mat::ones(5, 5, CV_32F) / 25;
	//filter2D(image, dst, -1, kernel);
	blur(image, dst, Size(5, 5));
	Mat edges;
	Canny(dst, edges, 25, 30);
	//imshow("ssss", dst);
	return edges;
}
//------------------END ----FUNCTION-----IMAGE_PREPROCESS()-------------------


//------------------START ----FUNCTION-----ADD_RAW_PIXEL()-------------------
set<int> add_rawpixel(Mat img)
{
	set<int> x;
	Mat image;
	img.convertTo(image, CV_8U);
	int col = image.cols, row = image.rows;

	int i, j,count;
	for ( i = 0; i < row; i++)
	{
		count = 0;
		for (j = 0; j< col; j++)
		{
			if (image.at<uchar>(i, j) > 0)
			{
				count++;
			}
		}
		if (count>0)
		{
			x.insert(count);
		}
	}
	//cout << "count=" << x[0];
	return x;
}
//------------------END ----FUNCTION-----ADD_RAW_PIXEL()-------------------


//------------------START ----FUNCTION-----EDGE_POSITION()-------------------
int edge_position(set<int> x1, set<int>x2)
{
	set <int> x;
	set_difference(x1.begin(), x1.end(), x2.begin(), x2.end(), std::inserter(x, x.begin()));
	int count_l = 1;
	//for (vector<int>::const_iterator i = x.begin(); i != x.end(); ++i)
	set<int> ::iterator i;
	for (i = x.begin(); i != x.end(); ++i)
	{
		// cout << *i << endl;
		if (*i > col1 / 2)
		{
			break;
		}
		count_l = count_l + 1;

		if (count_l < x.size())
		{
			return *i;
		}
		return 0;
	}
}
//------------------END ----FUNCTION-----EDGE_POSITION()-------------------

//------------------START ----FUNCTION-----LENGTH()-------------------
double length(int HeightPix)
{
	//	double f = 0.12;
	double fCamera_Height = 6.55;
	double fCamera_Angle_Vertical = 28.0;
	double fCamera_Angle_Horizontal = 0.0;
	double fSensor_Height = 0.0036;
	double fFocal_Length = 0.0169;
	double f = 0.165;
	double rows = 960;
	double sensorWidth = 0.048;
	double sensorHeight = 0.036;
	//	double alpha = 0.4677;
	double alpha = 0.48869;// 28 degrees
	double betaH = atan(sensorWidth / (2 * f));
	double height = 6.75; //in meter;
	double betaV = atan(fSensor_Height / (2 * fFocal_Length)); // In radians
	double y = double(rows - double(HeightPix)) / double(rows);
	double gamma = atan((2 * y - 1)*tan(betaV));
	double fCamera_Angle_Vertical_radians = fCamera_Angle_Vertical*0.0174532925;
	double distance = (fCamera_Height / tan(fCamera_Angle_Vertical_radians - gamma));
	//print str(HeightPix) + "\t" + str(distance)
	return distance;
}
//------------------END ----FUNCTION-----LENGTH()-------------------


//------------------START ----FUNCTION-----TAKESLICE()-------------------

Mat takeSlice(Mat img, int pos)
{
	
	int col = img.cols, row = img.rows;
	Mat slice;
	

	//Size sz = img.size();
	if (pos > 0 && pos < row)
	{
		for (int i = 0; i < col; ++i)
		{
			slice.at<uchar>(pos, i);  // ???
                        // slice.at<uchar>(0,i) = img.at<uchar>(pos, i); 
		}
	}
	return slice;
}

//------------------END ----FUNCTION-----TAKESLICE()-------------------		


//------------------START ----FUNCTION-----SEARCH_MATCH()-------------------

tuple<Point,Point> searchMatch(Mat img, Mat ref)
{
	int row = img.rows, col = img.cols;
	Mat imgf,refF,res,ims1;
	img.convertTo(imgf, CV_32F);
	ref.convertTo(refF, CV_32F);
	matchTemplate(imgf, refF, res, CV_TM_CCOEFF);
	double minVal, maxVal;
	Point minLoc, maxLoc,topLeft,bottomRight;
	minMaxLoc(res, &minVal, &maxVal, &minLoc, &maxLoc);
	topLeft = maxLoc;
	bottomRight = topLeft + Point(ref.cols,  ref.rows);
	rectangle(img, topLeft, bottomRight, 255, 1);
	resize(img, ims1,Size(640, 540));
	imshow("out2", ims1);
	return tuple<Point,Point>(minLoc, maxLoc);
}
//------------------END ----FUNCTION-----SEARCH_MATCH()-------------------



