import numpy as np
import csv
import time
import cv2
import math
col1=960
def length(HeightPix):      # IMPL      USED
    f = 0.12
    fCamera_Height = 6.55
    fCamera_Angle_Vertical = 28.0
    fCamera_Angle_Horizontal = 0.0
    fSensor_Height=  0.0036
    fFocal_Length = 0.0169
    f = 0.165
    rows=960
    sensorWidth = 0.048
    sensorHeight = 0.036
    alpha = 0.4677
    alpha = 0.48869 #// 28 degrees
    betaH= math.atan(sensorWidth / (2 * f))
    height = 6.75 #//in meter
    betaV =math.atan(fSensor_Height /(2 * fFocal_Length)); #// In radians
    y = float(rows-float(HeightPix)) / float(rows);
    gamma = math.atan((2 * y - 1)*math.tan(betaV));
    fCamera_Angle_Vertical_radians=fCamera_Angle_Vertical*0.0174532925
    distance = (fCamera_Height / math.tan(fCamera_Angle_Vertical_radians - gamma));
    #print str(HeightPix) +"\t"+str(distance)
    return distance

def takeSlice(img, pos): # take a slice of full width and 4 pixel ht  # IMPL USED
    shape = img.shape
    #print shape
    size  = img.size
    #print dtype
    if(pos > 0 and pos < shape[0]):
        slice = img[pos:,0:shape[1]]
    else:
        slice = None
    return slice

def searchMatch(img,ref):      # IMPL   USED
    shape = img.shape
    size  = img.size
    dtype = img.dtype
    w, h = ref.shape[1::-1]
    imgF = img.astype(np.float32);
    refF = ref.astype(np.float32);
    res = cv2.matchTemplate(imgF,refF, cv2.TM_CCOEFF)
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
    top_left = max_loc
    bottom_right = (top_left[0] + w, top_left[1] + h)
    cv2.rectangle(img, top_left, bottom_right, 255, 1)
    ims1=cv2.resize(img,(640,540))
    cv2.imshow('out2', ims1)
    #cv2.destroyWindow('image')
    return min_loc, max_loc

    
#resize the image
def add_rawpixel(img):               # IMPL   USED
    img=np.int16(img)
    col,row= img.shape
    data = np.zeros((col,5))
    for i in range(col):
        j=1;
        while (j <= 5):
            data[i,j-1]=np.sum(img[i,((j-1)*row/5):(j*row/5)])
            j=j+1
        
    #cv2.imshow("out1", data)
    x=[]
    for i in range(959,0,-1):
        if (data[i,1]!=0 or data[i,2]!=0 or data[i,3]!=0):
            x.append(i)
    print len(x)
    return x

#apply the gausion filter and edge deteion.
def image_preprocess(image):                    #IMPL   USED
    ims1=cv2.resize(image,(640,540))
    print image.shape
    print image.dtype
    cv2.imshow('input', ims1)
    kernel = np.ones((5,5),np.float32)/25    # was 11,11
    dst = cv2.filter2D(image,-1,kernel)
    edges = cv2.Canny(dst,25,30)
    ims=cv2.resize(edges,(640,540))
    cv2.imshow('out1', ims)
    cv2.waitKey(600)    # 330
    return edges

def edge_position(x1,x2):               # IMPL   USED
    x=set(x2)- set(x1);
    count_l=1;
    for i in x:
        if i > col1/2:break;
        count_l=count_l+1     
    if count_l<len(x):return i
    return 0

def edge_detection(listfile):          # 
    line1 = listfile.readline()
    file=line1[0:len(line1)-1]
    image1 = cv2.imread(dir+file)
    img1=image_preprocess(image1)
    x1=add_rawpixel(img1)
    while True:
        line2 = listfile.readline()
        if not line2: break
        file=line2[0:len(line2)-1]
        image2 = cv2.imread(dir+file)
        img2=image_preprocess(image2)
        x2=add_rawpixel(img2)
        a=edge_position(x1,x2)
        if (a!=0):
            return line1,line2,a
            break 
        else:
            x1=x2
            line1=line2
    return 0,0,0


#dir = 'Sample10/'
#listfile = open(dir+"file.lst")
listfile = open('imagedata1.txt')  # HBD open("images06.txt")
example=open('15hours_1.txt', 'w')
start=time.time()
line = listfile.readline()
file1=line[0:len(line)-1]
#P_image=cv2.imread(dir+file1)
P_image=cv2.imread(file1)
img1=image_preprocess(P_image)
x1=add_rawpixel(img1)
length_of_car=0
if(len(x1)<200):
        print "vechical not present in frame"+file1
flag=False
while True:
    print "time for frame no:"+file1+str(start-time.time())
    start=time.time()
    line = listfile.readline()
    if not line: break
    print line
    file2=line[0:len(line)-1]
    #s_image = cv2.imread(dir+file2)
    s_image = cv2.imread(file2)
    img2=image_preprocess(s_image)
    x2=add_rawpixel(img2)
    if(flag==False):
        if(len(x2)<200):
            print "vechical not present in frame"+file2 
        else:
            a=edge_position(x1,x2)
            if(a!=0):
                print "position of image" +str(a)
                l=length(a)-length(960)
                flag=True
        P_image=s_image
        img1=img2
        x1=x2
        file1=file2
    
    if(flag==True):
        if(len(x2)>500 ):
            print "current vechical can be car/or truck"
            max_len=len(x1)
            if (max_len<len(x2)):
                if(len(x2)<700):
                    print "its car"
                    for i in range(len(x2)-1):
                        t=x2[i]-x2[i-1]
                        if(x2[i]>500 and t>20):
                            maxpix=x2[i];
                        car=l+length(338)-length(x2[0])
                    print "length of car"+str(car)
                    if (length_of_car<car):
                        length_of_car=car
                        print "length of car"+str(length_of_car)
                        detail=file2+"\t"+str(length_of_car)+"\n"
                        print detail
                        example.writelines(detail)

        if(len(x2)>800):
                    print "its a truck/or Long vehicle"
                    l_truck=l+length(337)-length(960)
                    P_image=s_image
                    img1=img2
                    file1=file2
                    truck=file1    
                    refSlice = takeSlice(P_image, 940)  
                    length_estimate=0
                    count=1
                    while len(x2)>800:
                        line2 = listfile.readline()
                        if not line2: break
                        print line2
                        file2=line2[0:len(line2)-1]
                        #s_image = cv2.imread(dir+file2)
                        s_image = cv2.imread(file2)
                        img2=image_preprocess(s_image)
                        x2=add_rawpixel(img2)
                        min_loc,max_loc=searchMatch(s_image,refSlice)
                        length_estimate=length_estimate+(length(min_loc[1])-length(960))
                        P_image=s_image
                        img1=img2
                        file1=file2
                        x1=x2    
                        refSlice = takeSlice(P_image, 940) 
                        count=count+1
                    l_truck=l_truck+length_estimate/(count/2)
                    print "length of truck"+str(l_truck)
                    detail=truck+"\t"+str(l_truck)+"\n"
                    print detail
                    example.writelines(detail)

        else:
                if(len(x2)<200):
                    print "no vechical"
                    flag=False            
        P_image=s_image
        img1=img2
        x1=x2
        file1=file2
     
