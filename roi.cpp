#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "iostream"

using namespace cv;
using namespace std;

int main() 
{
    namedWindow("Example1");
    Mat img = imread("./Project/lena.jpg");
    if ( img.empty() ) { 
       cout << "No Image file" << endl;
       exit(-1); 
    }
    Mat roi = img( Rect(150,50,150,250) );
    imshow("Example1",roi);
    waitKey(0);
    return 0; // yea, c++, no cleanup nessecary!
}